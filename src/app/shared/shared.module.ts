import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { SpinnerComponent } from './component/spinner/spinner.component';


@NgModule({
  declarations: [
    SpinnerComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports:[SpinnerComponent]
})
export class SharedModule { }
