import { AuthService } from './../../services/auth.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable, exhaustMap, take } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthState } from 'src/app/auth/state/auth.state';
import { getToken } from 'src/app/auth/state/auth.selectors';

@Injectable()
export class HttpInterceptors implements HttpInterceptor {

  constructor(private store:Store<AuthState>) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // const token:any =  JSON.parse(localStorage.getItem('userData') || '{}').token;

    const contenttype = request.headers.get('Content-Type')

    return this.store.select(getToken).pipe(
      take(1),
      exhaustMap((token)=>{
        if(token){
          if(contenttype){
            const authReq = request.clone({
              headers: new HttpHeaders(
                {
                'Authorization': 'Bearer ' +token
              })
            });
            return next.handle(authReq);
          }
          else{
            const authReq = request.clone({
              headers: new HttpHeaders(
                {
                'Content-Type':  'application/json',
                'Authorization': 'Bearer ' +token
              })
            });
            return next.handle(authReq);
          }
        }
        else{
          request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        return next.handle(request);
        }
      })
    )

  }
  }

