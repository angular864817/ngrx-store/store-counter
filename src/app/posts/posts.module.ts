import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { counterReduer } from '../counter/state/counter.reducer';
import { POSTS_STATE_NAME } from './state/posts.selectors';
import { postsReduer } from './state/posts.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PostsEffects } from './state/posts.effects';
import { SinglepostComponent } from './singlepost/singlepost.component';
import { PostslistComponent } from './postslist/postslist.component';


@NgModule({
  declarations: [
    SinglepostComponent,
    PostslistComponent
  ],
  imports: [
    CommonModule,
    PostsRoutingModule,
    StoreModule.forFeature(POSTS_STATE_NAME,postsReduer),
    EffectsModule.forFeature([PostsEffects])
  ]
})
export class PostsModule { }
