import { CounterComponent } from './../counter/counter/counter.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPostComponent } from './postslist/add-post/add-post.component';
import { UpdatePostComponent } from './postslist/update-post/update-post.component';
import { PostslistComponent } from './postslist/postslist.component';
import { SinglepostComponent } from './singlepost/singlepost.component';

const routes: Routes = [
  {
    path: 'posts',
    component: PostslistComponent,
    children: [
      {
        path: 'add-post',
        component: AddPostComponent,
      },
      {
        path: 'update-post/:id',
        component: UpdatePostComponent,
      },
    ],
  },
  {
    path: 'posts/details/:id',
    component: SinglepostComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsRoutingModule {}
