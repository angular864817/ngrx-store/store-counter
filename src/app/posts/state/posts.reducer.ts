import {
  addPost,
  addPostSuccess,
  deletePost,
  deletePostSucess,
  loadPostsSucess,
  updatePost,
  updatePostSuccess,
} from './posts.action';
import { Action, createReducer, on } from '@ngrx/store';
import { initialstate, postAdapter, poststate } from './posts.stats';

const _postsReducer = createReducer(
  initialstate,

  // for local add update and delete perform

  // on(addPost,(state,action)=>{
  //   let post = {...action.post};

  //   post.id = (state.posts.length+1).toString();

  //   return {
  //     ...state,
  //     posts:[...state.posts,post]
  //   }
  // }),

  // on(updatePost,(state,action)=>{
  //   const updateposts = state.posts.map((post: { _id: string | undefined; })=>{
  //     return action.post._id === post._id ? action.post:post;
  //   })

  //   return {
  //     ...state,
  //     posts:updateposts
  //   }
  // }),

  // on(deletePost,(state,{_id})=>{
  //   const updateposts = state.posts.filter((post: { _id: string; })=>{
  //     return post._id !== _id;
  //   })

  //   return {
  //     ...state,
  //     posts:updateposts
  //   }
  // }),

  // with api integration use like this store with out entity

  // on(addPostSuccess,(state,action)=>{
  //   let post = {...action.post};
  //   return {
  //     ...state,
  //     posts:[...state.posts,post]
  //   }
  // }),

  // on(updatePostSuccess,(state,action)=>{
  //   const updateposts = state.posts.map((post:any)=>{
  //     return action?.post?._id === post._id ? action.post:post;
  //   })

  //   return {
  //     ...state,
  //     posts:updateposts
  //   }
  // }),
  // on(deletePostSucess,(state,{_id})=>{
  //   const updateposts = state.posts.filter((post: { _id: string; })=>{
  //     return post._id !== _id;
  //   })

  //   return {
  //     ...state,
  //     posts:updateposts
  //   }
  // }),

  // on(loadPostsSucess,(state,action)=>{
  //   return {
  //     ...state,
  //     posts:action.posts
  //   }
  // })

  // with use of entity
  on(addPostSuccess, (state, action) => {
    return postAdapter.addOne(action.post,
      {
        ...state,
        count:state.count+1,
      });
  }),

  on(updatePostSuccess, (state, action) => {
    return postAdapter.updateOne(action.post, state);
  }),
  on(deletePostSucess, (state, { _id }) => {
    return postAdapter.removeOne(_id, state);
  }),

  on(loadPostsSucess, (state, action) => {
    return postAdapter.setAll(action.posts, {
      ...state,
      count:state.count+1,
    });
  })
);

export function postsReduer(state: poststate | undefined, action: Action) {
  return _postsReducer(state, action);
}
