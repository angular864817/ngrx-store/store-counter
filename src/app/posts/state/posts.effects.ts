import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  exhaustMap,
  filter,
  map,
  mergeMap,
  of,
  switchMap,
  withLatestFrom,
} from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { setLoadingSpinner } from 'src/app/store/shared/shared.action';
import { Router, Params } from '@angular/router';
import { PostsService } from 'src/app/services/posts.service';
import {
  loadPosts,
  loadPostsSucess,
  addPost,
  addPostSuccess,
  updatePost,
  updatePostSuccess,
  deletePost,
  deletePostSucess,
} from './posts.action';
import { ROUTER_NAVIGATION, RouterNavigationAction } from '@ngrx/router-store';
import { Update } from '@ngrx/entity';
import { Post } from 'src/app/models/posts.model';
import { getposts } from './posts.selectors';
import { dummyAction } from 'src/app/auth/state/auth.actions';

@Injectable()
export class PostsEffects {
  constructor(
    private actions$: Actions,
    private postsService: PostsService,
    private store: Store<AppState>,
    private router: Router
  ) {}

  loadPosts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadPosts),
      withLatestFrom(this.store.select(getposts)),
      mergeMap(([action, posts]) => {
        if (!posts.length || posts.length === 1) {
          return this.postsService.getPosts().pipe(
            map((posts) => {
              return loadPostsSucess({ posts });
            })
          );
        }
        return of(dummyAction());
      })
    );
  });

  addPost$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(addPost),
      mergeMap((action) => {
        return this.postsService.addPost(action.post).pipe(
          map((data) => {
            const post = { ...action.post, _id: data.data._id };
            return addPostSuccess({ post });
          })
        );
      })
    );
  });

  updatePost$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(updatePost),
      switchMap((action) => {
        const { _id, ...newCreateObject } = action.post;
        return this.postsService
          .updatePost(action.post._id, newCreateObject)
          .pipe(
            map((data) => {
              const updatedPost: Update<Post> = {
                id: action.post._id,
                changes: {
                  ...action.post,
                },
              };
              return updatePostSuccess({ post: updatedPost });

              // without use of entity
              // return updatePostSuccess({ post: action.post });
            })
          );
      })
    );
  });

  deletePost$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(deletePost),
      switchMap((action) => {
        return this.postsService.deletePost(action._id).pipe(
          map((data) => {
            return deletePostSucess({ _id: action._id });
          })
        );
      })
    );
  });

  getSinglePost$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROUTER_NAVIGATION),
      filter((r: RouterNavigationAction) => {
        return r.payload.routerState.url.startsWith('/post/posts/details');
      }),
      map((r: RouterNavigationAction | any) => {
        return r.payload.routerState['params']['id'];
      }),
      withLatestFrom(this.store.select(getposts)),
      switchMap(([id, posts]) => {
        if (!posts.length) {
          return this.postsService.getPostById(id).pipe(
            map((post) => {
              const postData = [{ ...post.data }];
              return loadPostsSucess({ posts: postData });
            })
          );
        }
        return of(dummyAction());
      })
    );
  });
}
