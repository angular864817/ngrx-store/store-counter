import { Post } from 'src/app/models/posts.model';
import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

export const ADD_POST_ACTION = '[post page] add post';
export const ADD_POST_SUCCESS = '[post page] add post sucess';

export const UPDATE_POST_ACTION = '[post page] update post';
export const UPDATE_POST_SUCCESS = '[post page] update post success';

export const DELETE_POST_ACTION = '[post page] delete post';
export const DELETE_POST_SUCCESS = '[post page] delete post success';

export const LOAD_POSTS = '[post page] load post';
export const LOAD_POSTS_SUCCESS = '[post page] load post success';


export const addPost = createAction(
  ADD_POST_ACTION,
  props<{ post: any }>());

export const addPostSuccess = createAction(
  ADD_POST_SUCCESS,
  props<{ post: any }>()
);

export const updatePost = createAction(
  UPDATE_POST_ACTION,
  props<{ post: any }>()
);

// without use of entity
// export const updatePostSuccess = createAction(
//   UPDATE_POST_SUCCESS,
//   props<{ post: any }>()
// );

// with use of entity
export const updatePostSuccess = createAction(
  UPDATE_POST_SUCCESS,
  props<{ post: Update<Post>}>()
);

export const deletePost = createAction(
  DELETE_POST_ACTION,
  props<{ _id: string }>()
);

export const deletePostSucess = createAction(
  DELETE_POST_SUCCESS,
  props<{ _id: string }>()
);

export const loadPosts = createAction(LOAD_POSTS);
export const loadPostsSucess = createAction(
  LOAD_POSTS_SUCCESS,
  props<{ posts: any }>()
);

