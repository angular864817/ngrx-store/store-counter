import { RouterStateUrl } from './../../store/router/custom-serializer';
import { Post } from 'src/app/models/posts.model';
import { postAdapter, poststate } from './posts.stats';
import { createFeatureSelector ,createSelector} from "@ngrx/store";
import { getCurrentRoute } from 'src/app/store/router/router.selector';

export const POSTS_STATE_NAME = 'posts';

const getpostsstate = createFeatureSelector<poststate>(POSTS_STATE_NAME);


// export const getposts = createSelector(getpostsstate, (state)=>{
//   return state.posts
// })

// for local use
// export const getpostbyid = createSelector(getpostsstate, (state:poststate,props:any)=>{
//   return state.posts.find((post:Post)=>post._id === props.id)
// })

// without use of entity
// export const getpostbyid = createSelector(getposts,getCurrentRoute,
//    (posts,route:RouterStateUrl)=>{
//   return posts ? posts.find((post:Post)=>post._id ===route.params['id']) :null
// })

// with use of entity
export const postSelectors = postAdapter.getSelectors();

export const getposts = createSelector(getpostsstate, postSelectors.selectAll);

export const getPostEntities = createSelector(
  getpostsstate,
  postSelectors.selectEntities
);

export const getpostbyid = createSelector(getPostEntities,getCurrentRoute,
  (posts,route:RouterStateUrl)=>{
    return posts ? posts[route.params['id']] : null
  }
)

export const getCount = createSelector(getpostsstate, (state) => state.count);
