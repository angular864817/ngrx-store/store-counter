import { Post } from './../../models/posts.model';
import { EntityState, createEntityAdapter } from '@ngrx/entity';

// with out use of ngrx entity

// export interface poststate {
//   posts : any
//   // posts : Post[]
// }

// export const initialstate:poststate = {
//   posts:null
// }


// with use of ngrx entity

export interface poststate extends EntityState<Post>{
  count: number;
};

export const postAdapter  = createEntityAdapter<Post>({
  selectId:(post:Post)=>post._id,
  sortComparer: sortByName,
});

export const initialstate:poststate = postAdapter.getInitialState(
  // if you have initial data then it will give here {id:"dk",title:"swdef"}
  {
    count: 0,
  }
);

export function sortByName(a: Post, b: Post): number {
  const compare = a.description.localeCompare(b.description);
  if (compare > 0) {
    return -1;
  }

  if (compare < 0) {
    return 1;
  }

  return compare;
}
