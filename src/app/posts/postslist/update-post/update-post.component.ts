import { updatePost } from './../../state/posts.action';
import { Subscription } from 'rxjs';
import { getpostbyid } from './../../state/posts.selectors';
import { Post } from 'src/app/models/posts.model';
import { AppState } from './../../../store/app.state';
import { Store } from '@ngrx/store';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.css'],
})
export class UpdatePostComponent {
  postUpdateForm!: FormGroup;
  post!: Post;
  postsubscription!: Subscription;

  constructor(
    private Fb: FormBuilder,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private router:Router
  ) {}



  ngOnInit(): void {
    // this.route.paramMap.subscribe((params) => {
    //   const id = params.get('id');

    //   this.postsubscription = this.store
    //     .select(getpostbyid, { id })
    //     .subscribe((data) => {
    //       this.post = data;
    //       this.cerateform();
    //     });
    // });

    this.cerateform();
    this.postsubscription = this.store.select(getpostbyid).subscribe((post)=>{
      if(post){
      this.post = post;
      this.postUpdateForm.patchValue({
        description:post.description,
        completed:post.completed
      });
    }
    })
  }

  cerateform() {
    this.postUpdateForm = new FormGroup({
      description: new FormControl(null, [Validators.required]),
      completed: new FormControl(null, [Validators.required]),
    });
  }

  postUpdateform(data: any) {

    if(this.postUpdateForm.valid){
    const post:Post = {
      _id: this.post._id,
      description  : this.postUpdateForm.value.description,
      completed :  this.postUpdateForm.value.completed
    }
    this.store.dispatch(updatePost({post}));
    this.router.navigate(['/post/posts'])
  }
}

  ngOnDestroy(): void {
    if (this.postsubscription) {
      this.postsubscription.unsubscribe();
    }
  }
}
