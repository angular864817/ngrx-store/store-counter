import { addPost } from './../../state/posts.action';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from 'src/app/models/posts.model';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent {

  postForm:FormGroup
  constructor(private fb:FormBuilder,
    private store:Store<AppState>){

    this.postForm = this.fb.group({
      description:['',[Validators.required]],
      completed:['',[Validators.required]]
    })
  }

  postform(data:any){
    if(this.postForm.valid){
      const post:any = {
        description:data.description,
        completed : data.completed
      }

      this.store.dispatch(addPost({post}));
    }

  }

}
