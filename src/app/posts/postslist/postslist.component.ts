import { Observable } from 'rxjs';
import { AppState } from './../../store/app.state';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { Post } from 'src/app/models/posts.model';
import { getCount, getposts } from '../state/posts.selectors';
import { deletePost, loadPosts } from '../state/posts.action';

@Component({
  selector: 'app-postslist',
  templateUrl: './postslist.component.html',
  styleUrls: ['./postslist.component.css']
})
export class PostslistComponent {

  posts!:Observable<any>;
  count!: Observable<number>;

  constructor(private store:Store<AppState>){

  }
  ngOnInit(): void {
    this.posts = this.store.select(getposts);
    this.count = this.store.select(getCount);
    this.store.dispatch(loadPosts())
  }

  deletepost(_id:any){
    this.store.dispatch(deletePost({_id}))
  }

}
