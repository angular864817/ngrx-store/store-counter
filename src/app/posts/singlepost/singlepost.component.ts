import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { Post } from 'src/app/models/posts.model';
import { AppState } from 'src/app/store/app.state';
import { getpostbyid } from '../state/posts.selectors';

@Component({
  selector: 'app-singlepost',
  templateUrl: './singlepost.component.html',
  styleUrls: ['./singlepost.component.css']
})
export class SinglepostComponent {

  post!:Observable<any>;

  constructor(private store:Store<AppState>){}

  ngOnInit(): void {
    this.post = this.store.select(getpostbyid);
    // this.store.dispatch(loadPosts())
  }

}
