import { getLoading, getErrorMessage } from './store/shared/shared.selector';
import { AppState } from 'src/app/store/app.state';
import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { autoLogin } from './auth/state/auth.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'store-counter';

  showLoading!:Observable<boolean>;
  showError!:Observable<string>;
  constructor(private store:Store<AppState>){}

  ngOnInit(): void {
    this.showLoading = this.store.select(getLoading);
    this.showError = this.store.select(getErrorMessage);
    this.store.dispatch(autoLogin())
  }
}
