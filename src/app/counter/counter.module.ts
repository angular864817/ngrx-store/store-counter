import { Store, StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CounterRoutingModule } from './counter-routing.module';
import { counterReduer } from './state/counter.reducer';
import { COUNTER_STATE_NAME } from './state/counter.selectors';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CounterRoutingModule,
    StoreModule.forFeature(COUNTER_STATE_NAME,counterReduer)
  ]
})
export class CounterModule { }
