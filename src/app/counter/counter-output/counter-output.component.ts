import { AppState } from './../../store/app.state';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { getcounter } from '../state/counter.selectors';

@Component({
  selector: 'app-counter-output',
  templateUrl: './counter-output.component.html',
  styleUrls: ['./counter-output.component.css']
})
export class CounterOutputComponent {

  counter!:number;

  // countersubscribe!:Subscription;

  counter$!:Observable<number>

  constructor(private store:Store<AppState>){}

  ngOnInit(): void {
    // this.countersubscribe = this.store.select(getcounter).subscribe(counter=>{
    //   console.log("counter call")
    //   this.counter = counter
    // })


    this.counter$ = this.store.select(getcounter)
  }

  ngOnDestroy(): void {
    // if(this.countersubscribe){
    //   this.countersubscribe.unsubscribe()
    // }
  }

}
