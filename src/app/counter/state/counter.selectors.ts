import { createFeatureSelector,createSelector } from "@ngrx/store";
import { counterstate } from "./counter.state";

export const COUNTER_STATE_NAME = 'counter';

const getcounterstate = createFeatureSelector<counterstate>(COUNTER_STATE_NAME);


export const getcounter = createSelector(getcounterstate, (state)=>{
  return state.counter
})

export const getchammelname = createSelector(getcounterstate, (state)=>{
  return state.channelname
})
