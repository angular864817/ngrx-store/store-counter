import { state } from '@angular/animations';
import { increment, decrement, reset, customincrement, chnagechannelname } from './counter.action';
import { initialstate } from './counter.state';
import { Action, createReducer, on } from "@ngrx/store";

const _counterReducer  = createReducer(
  initialstate,
  on(increment,(state)=>{
    return {
      ...state,
      counter:state.counter+1
    }
  }),
  on(decrement,(state)=>{
    return {
      ...state,
      counter:state.counter-1
    }
  }),
  on(reset,(state)=>{
    return {
      ...state,
      counter:0
    }
  }),
  on(customincrement,(state,Action)=>{
    return {
      ...state,
      counter:Action.count+state.counter
    }
  }),
  on(chnagechannelname,(state)=>{
    return {
      ...state,
      channelname:"sahil ajudiya"
    }
  })
)


export function counterReduer(state: { counter: number;channelname:string} | undefined  ,action: Action){
  return _counterReducer(state,action);
}
