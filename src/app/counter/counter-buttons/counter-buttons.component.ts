import { AppState } from './../../store/app.state';
import { increment, decrement, reset } from './../state/counter.action';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-counter-buttons',
  templateUrl: './counter-buttons.component.html',
  styleUrls: ['./counter-buttons.component.css']
})
export class CounterButtonsComponent {

  constructor(private store:Store<AppState>){}

  onincrement(){
    this.store.dispatch(increment())
  }
  ondecrement(){
    this.store.dispatch(decrement())
  }
  onreset(){
    this.store.dispatch(reset())
  }
}
