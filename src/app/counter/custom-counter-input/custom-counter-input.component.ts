import { AppState } from './../../store/app.state';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { chnagechannelname, customincrement } from '../state/counter.action';
import { getchammelname } from '../state/counter.selectors';

@Component({
  selector: 'app-custom-counter-input',
  templateUrl: './custom-counter-input.component.html',
  styleUrls: ['./custom-counter-input.component.css']
})
export class CustomCounterInputComponent {

  value!:number;

  channelname!:string;

  constructor(private store:Store<AppState>){}

  add(){
    this.store.dispatch(customincrement({count: +this.value}))
  }
  ngOnInit(): void {
    this.store.select(getchammelname).subscribe(channelname=>{
      console.log("channel call")
      this.channelname = channelname
    })

  }

  chnagetext(){
    this.store.dispatch(chnagechannelname())
  }
}
