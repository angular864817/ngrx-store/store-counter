import { on } from '@ngrx/store';
import { AuthState, initialstate } from './auth.state';
import { Action, createReducer } from '@ngrx/store';
import { autoLogout, loginsucess, signupsucess } from './auth.actions';


const _authReducer = createReducer(
  initialstate,
  on(loginsucess,(state,action)=>{
    return {
      ...state,
      user:action.user
    }
  }),
  on(signupsucess,(state,action)=>{
    return {
      ...state,
      user:action.user
    }
  }),
  on(autoLogout,(state)=>{
    return{
      ...state,
      user:null
    }
  })
  )

export function AuthReducer(state: any,action: Action){
  return _authReducer(state,action);
}
