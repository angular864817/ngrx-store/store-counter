import { User } from './../../models/user.model';
import { props } from '@ngrx/store';
import { createAction } from '@ngrx/store';


export const LOGIN_START = '[auth page] login start';
export const LOGIN_SUCCESS = '[auth page] login sucess';
export const LOGIN_FAIL = '[auth page] login fail';

export const SIGNUP_START = '[auth page] signup start';
export const SIGNUP_SUCCESS = '[auth page] signup sucess';

export const AUTO_LOGIN_ACTION = '[auth page] auto login';
export const LOGOUT_ACTION = '[auth page] logout';

export const loginstart = createAction(
  LOGIN_START,
  props<{email:string,password:string}>()
);

export const loginsucess = createAction(
  LOGIN_SUCCESS,
  props<{user:User,redirect:boolean}>()
  );

export const signupstart = createAction(
    SIGNUP_START,
    props<{email:string,password:string}>()
  );

export const signupsucess = createAction(
    SIGNUP_SUCCESS,
    props<{user:User,redirect:boolean}>()
);

export const autoLogin = createAction(AUTO_LOGIN_ACTION);

export const autoLogout = createAction(LOGOUT_ACTION);

export const dummyAction = createAction('[dummy action]');




