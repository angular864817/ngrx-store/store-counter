import { setErrorMessage } from './../../store/shared/shared.action';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import {
  autoLogin,
  autoLogout,
  loginstart,
  loginsucess,
  signupstart,
  signupsucess,
} from './auth.actions';
import { AuthService } from './../../services/auth.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { setLoadingSpinner } from 'src/app/store/shared/shared.action';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router
  ) {}

  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loginstart),
      exhaustMap((action) => {
        return this.authService
          .login(action.email, action.password)
          .pipe(
            map((data) => {
              const user = this.authService.formatUser(data);
              this.authService.setUserInLocalStorage(user);
              this.store.dispatch(setLoadingSpinner({ status: false }));
              return loginsucess({ user,redirect:true });
            }),
            catchError((error) => {
              this.store.dispatch(setLoadingSpinner({ status: false }));
              const errorMessage = error.error.message;
              this.store.dispatch(setErrorMessage({ message: errorMessage }));
              return of();
            })
          );
      })
    );
  });

  loginRedirect$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(...[loginsucess, signupsucess]),
        tap((action) => {
          if(action.redirect){
          this.router.navigate(['/']);}
        })
      );
    },
    {
      dispatch: false,
    }
  );

  signUp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(signupstart),
      exhaustMap((action) => {
        return this.authService
          .signUp(action.email, action.password)
          .pipe(
            map((data) => {
              this.store.dispatch(setLoadingSpinner({ status: false }));
              const user = this.authService.formatUser(data);
              return signupsucess({ user,redirect:true });
            }),
            catchError((error) => {
              this.store.dispatch(setLoadingSpinner({ status: false }));
              const errorMessage = error.error.message;
              this.store.dispatch(setErrorMessage({ message: errorMessage }));
              return of();
            })
          );
      })
    );
  });

  autoLogin$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(autoLogin),
        mergeMap((action) => {
          const user = this.authService.getUserFromLocalStorage();
          if (user) {
            return of(this.store.dispatch(loginsucess({user,redirect:false})));
          }
          return of();
        })
      );
    },
    {
      dispatch: false,
    }
  );

  logout$ = createEffect(()=>{
    return this.actions$.pipe(
      ofType(autoLogout),
      map((action)=>{
        this.authService.logout();
        this.router.navigate(['/auth'])
      })
    )
  },
  {
    dispatch:false
  })
}
