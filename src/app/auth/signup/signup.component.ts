import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from 'src/app/store/app.state';
import { setLoadingSpinner } from 'src/app/store/shared/shared.action';
import { signupstart } from '../state/auth.actions';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  signUpForm!: FormGroup;

  constructor(private fb:FormBuilder,
    private store:Store<AppState>){}

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      email:['',[Validators.required]],
      password:['',[Validators.required]]
    })
  }

  signUpFormSubmit(data:any){
    const email =  data.company_email;
    const password = data.password;

    this.store.dispatch(setLoadingSpinner({status:true}));
    this.store.dispatch(signupstart({email,password}))
  }

}
