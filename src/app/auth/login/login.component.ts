import { setLoadingSpinner } from './../../store/shared/shared.action';
import { loginstart } from '../state/auth.actions';
import { AppState } from 'src/app/store/app.state';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loginForm:FormGroup;

  constructor(private fb:FormBuilder,
    private store:Store<AppState>){
    this.loginForm = this.fb.group({
      email:['',[Validators.required]],
      password:['',[Validators.required]]
    })
  }

  loginform(data:any){
    if(this.loginForm.valid){
      const email = data.email
      const password = data.password
      this.store.dispatch(setLoadingSpinner({status:true}))
    this.store.dispatch(loginstart({email,password}))
    }
  }

}
