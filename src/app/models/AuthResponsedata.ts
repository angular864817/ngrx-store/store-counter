export interface AuthResponseData{
    status: number,
    message: string,
    data: {
      user: {
        _id: string,
        first_name: string,
      last_name: string
      email: string,
      personal_email: string,
        department: number,
        user_type: number,
        employee_status: number,
        created_at: number,
        joining_date: number,
        updated_at: number,
        __v: number
      },
      token: string
    }
  }

