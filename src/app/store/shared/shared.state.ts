
export interface SharedState {
  showLoading:boolean;
  showErrorMessage:string
}

export const initialstate:SharedState = {
  showLoading:false,
  showErrorMessage:''
}
