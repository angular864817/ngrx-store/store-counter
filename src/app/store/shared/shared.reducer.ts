import { Action } from '@ngrx/store';
import { setLoadingSpinner, setErrorMessage } from './shared.action';
import { on } from '@ngrx/store';
import { createReducer } from '@ngrx/store';
import { initialstate, SharedState } from './shared.state';


const _sharedReducer = createReducer(
  initialstate,
  on(setLoadingSpinner,(state,action)=>{
    return {
      ...state,
      showLoading:action.status
    }
  }),
  on(setErrorMessage,(state,action)=>{
    return {
      ...state,
      showErrorMessage:action.message
    }
  })
)

export function SharedReducer(state:any,action: Action){
  return _sharedReducer(state,action)
}
