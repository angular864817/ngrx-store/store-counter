import { User } from './../models/user.model';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, catchError, throwError, Observable } from "rxjs";
import { environment } from "src/environments/environments";
import { AuthResponseData } from "../models/AuthResponsedata";


@Injectable({
  providedIn:'root',
})

export class AuthService{

  constructor(private http:HttpClient){}

  // Login User Api
 login(email:string,password:string):Observable<AuthResponseData> {
  return this.http.post<AuthResponseData>(environment.apiUrl+'user/login',{email,password}).pipe(
    map((res: any) => {
        return res;
    }),
  );
}
  // sign up User Api
signUp(email:string,password:string):Observable<AuthResponseData> {
 return this.http.post<AuthResponseData>(environment.apiUrl+'user/login',{email,password}).pipe(
   map((res: any) => {
       return res;
   }),
 );
}

formatUser(data:AuthResponseData){
  const user = new User(data.data.user.email,data.data.token)
  return user
}

setUserInLocalStorage(user:User){
  localStorage.setItem('userData',JSON.stringify(user));
}

getUserFromLocalStorage(){
  const userDataString  = localStorage.getItem('userData');
  if(userDataString){
    const userData = JSON.parse(userDataString);
    const user = new User(userData.email,userData.token)
    return user
  }
  return null
}

logout(){
  localStorage.removeItem('userData');
}
}
