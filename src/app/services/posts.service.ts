import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http:HttpClient) { }

    getPosts(){
      return this.http.get(environment.apiUrl+'task/list').pipe(
        map((res: any) => {
            return res.data;
        }),
      )
    }

  addPost(post:any){
    return this.http.post(environment.apiUrl+'task/',post).pipe(
      map((res: any) => {
          return res;
      }),
    )
  }

  updatePost(id:any,post:any){
    return this.http.patch(environment.apiUrl+'task/update/'+id,post).pipe(
      map((res: any) => {
          return res;
      }),
    )
  }

  deletePost(id:any){
    return this.http.delete(environment.apiUrl+'task/delete/'+id).pipe(
      map((res: any) => {
          return res;
      }),
    )
  }

  getPostById(id:any){
    return this.http.get(environment.apiUrl+'task/'+id).pipe(
      map((res: any) => {
          return res;
      }),
    )
  }
}
